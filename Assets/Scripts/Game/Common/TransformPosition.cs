﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public class TransformPositionParams : IModelParams {
        public Transform Transform { get; private set; }
        public TransformPositionParams(Transform transform) {
            Transform = transform;
        }
    }

    public class TransformPosition : Model<TransformPositionParams>, IPositionAdapter
    {
        private Transform _transform;

        protected override void Init(TransformPositionParams modelParams)
        {
            base.Init(modelParams);
            _transform = modelParams.Transform;
        }

        public void Set(Vector3 position) {
            _transform.localPosition = position;
        }

        public void SetImmediate(Vector3 position)
        {
            Set(position);
        }

        public void Translate(Vector3 offset) {
            _transform.Translate(offset, Space.Self);
        }

        public Vector3 Get() {
            return _transform.localPosition;
        }
    }
}

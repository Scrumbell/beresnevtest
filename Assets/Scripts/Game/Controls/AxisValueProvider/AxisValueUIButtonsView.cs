﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.UI.Common;

namespace BeresnevTest.Game.Controls.UI
{
    public class AxisValueUIButtonsView : View
    {
        public PointerElement NegativeButton;
        public PointerElement PositiveButton;
    }
}

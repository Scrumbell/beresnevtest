﻿using BeresnevTest.Core.Mvc;

namespace BeresnevTest.Game.Controls
{
    public interface IAxisValueProvider
    {
        AxisValue GetAxisValue();
        float GetAxisValueFloat();
    }

    public abstract class AxisValueProvider<TParams> : Model<TParams>, IAxisValueProvider where TParams : IModelParams
    {
        public abstract AxisValue GetAxisValue();

        public float GetAxisValueFloat()
        {
            return (float)GetAxisValue();
        }
    }
}

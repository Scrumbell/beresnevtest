﻿using System.Collections.Generic;
using System.Reflection;

namespace BeresnevTest.Core.Injection
{

	[System.AttributeUsage(System.AttributeTargets.Field)]
	public sealed class Inject : System.Attribute
	{
	}

	public class InjectorException : System.Exception
	{
		public InjectorException(string message) : base(message)
		{
		}
	}

	public interface IInjector {
		void Map<T>(T value);
		void Unmap<T>();
		void Unmap(object obj);
		void InjectInto(object obj);
	}

	public class Injector : IInjector
	{
		public Injector()
		{

		}

		private readonly Dictionary<System.Type, object> _injections = new Dictionary<System.Type, object>();
		private bool _postMappingDone = false;

		public void Map<T>(T value)
		{
			var type = typeof(T);
			_injections[type] = value;
			if (_postMappingDone) {
				InjectInto(value);
			}
		}

		public void Unmap<T>()
        {
			if (_injections.ContainsKey(typeof(T))) {
				_injections.Remove(typeof(T));
			}
		}

		public void Unmap(object obj)
		{
			if (_injections.ContainsKey(obj.GetType()))
			{
				_injections.Remove(obj.GetType());
			}
		}

		public void InjectInto(object obj)
		{
			var fields = Reflector.Reflect(obj.GetType());
			var fieldsLength = fields.Length;
			for (var i = 0; i < fieldsLength; i++)
			{
				var field = fields[i];
				var value = Get(field.FieldType);
				field.SetValue(obj, value);
			}
		}

		private object Get(System.Type type)
		{
			object obj;

			if (!_injections.TryGetValue(type, out obj))
			{
				throw new InjectorException(type.FullName  + " not present in injector");
			}

			return obj;
		}

		public void PostMapping()
		{
			if (!_postMappingDone)
			{
				foreach (var objKvp in _injections)
				{
					InjectInto(objKvp.Value);
				}
				_postMappingDone = true;
			}
		}

		private static class Reflector
		{
			private static readonly System.Type _injectAttributeType = typeof(Inject);
			private static readonly Dictionary<System.Type, FieldInfo[]> cachedFieldInfos = new Dictionary<System.Type, FieldInfo[]>();

			public static FieldInfo[] Reflect(System.Type type)
			{

				FieldInfo[] cachedResult;
				if (cachedFieldInfos.TryGetValue(type, out cachedResult))
				{
					return cachedResult;
				}

				List<FieldInfo> result = new List<FieldInfo>(512);

				var fields = type.GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
				for (var fieldIndex = 0; fieldIndex < fields.Length; fieldIndex++)
				{
					var field = fields[fieldIndex];
					var hasInjectAttribute = field.IsDefined(_injectAttributeType, inherit: false);
					if (hasInjectAttribute)
					{
						result.Add(field);
					}
				}
				var resultAsArray = result.ToArray();
				cachedFieldInfos[type] = resultAsArray;
				return resultAsArray;
			}
		}

		public void Destroy() {
			_injections.Clear();
		}
	}
}
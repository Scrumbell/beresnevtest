﻿using UnityEngine;

namespace BeresnevTest.Core.Mvc
{
    public interface IView {
        void Show();
        void Hide();
        void Destroy();
    }
    public class View : MonoBehaviour, IView
    {
        public void Show() {
            gameObject.SetActive(true);
        }

        public void Hide() {
            gameObject.SetActive(false);
        }

        public void Destroy()
        {
            if (this != null)
            {
                Object.Destroy(gameObject);
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace BeresnevTest.Core.UpdateManagement {
    public interface IUpdateManager {
        void AddUpdateAction(Action<float> action);
        void AddFixedUpdateAction(Action<float> action);
        void RemoveUpdateAction(Action<float> action);
        void RemoveFixedUpdateAction(Action<float> action);
        void ClearAllActions();
    }

    public class UpdateManager : MonoBehaviour, IUpdateManager {
        private List<Action<float>> _updateActions;
        private List<Action<float>> _toAddUpdateActions;
        private List<Action<float>> _toRemoveUpdateActions;
        private List<Action<float>> _fixedUpdateActions;
        private List<Action<float>> _toAddFixedUpdateActions;
        private List<Action<float>> _toRemoveFixedUpdateActions;

        private bool _clearAllActions = false;

        private void Awake()
        {
            _updateActions = new List<Action<float>>();
            _toAddUpdateActions = new List<Action<float>>();
            _toRemoveUpdateActions = new List<Action<float>>();

            _fixedUpdateActions = new List<Action<float>>();
            _toAddFixedUpdateActions = new List<Action<float>>();
            _toRemoveFixedUpdateActions = new List<Action<float>>();
        }

        public void AddUpdateAction(Action<float> action)
        {
            _toAddUpdateActions.Add(action);
        }

        public void RemoveUpdateAction(Action<float> action)
        {
            _toAddUpdateActions.RemoveAll(updateAction => updateAction == action);
            _toRemoveUpdateActions.Add(action);
        }

        private void Update()
        {
            foreach (var action in _toRemoveUpdateActions)
            {
                for (int i = _updateActions.Count - 1; i >= 0; i--)
                {
                    if (_updateActions[i] == action) _updateActions.RemoveAt(i);
                }
            }
            _toRemoveUpdateActions.Clear();

            foreach (var action in _toAddUpdateActions)
            {
                _updateActions.Add(action);
            }
            _toAddUpdateActions.Clear();

            foreach (var updateAction in _updateActions)
            {
                if (_clearAllActions) break;
                if (_toRemoveUpdateActions.Contains(updateAction)) continue;

                updateAction?.Invoke(Time.deltaTime);
            }

            if (_clearAllActions)
            {
                ClearAllActionsComplete();
            }
        }

        public void AddFixedUpdateAction(Action<float> action)
        {
            _toAddFixedUpdateActions.Add(action);
        }

        public void RemoveFixedUpdateAction(Action<float> action)
        {
            _toAddFixedUpdateActions.RemoveAll(updateAction => updateAction == action);
            _toRemoveFixedUpdateActions.Add(action);
        }

        private void FixedUpdate()
        {
            foreach (var action in _toRemoveFixedUpdateActions)
            {
                for (int i = _fixedUpdateActions.Count - 1; i >= 0; i--)
                {
                    if (_fixedUpdateActions[i] == action) _fixedUpdateActions.RemoveAt(i);
                }
            }
            _toRemoveFixedUpdateActions.Clear();

            foreach (var action in _toAddFixedUpdateActions)
            {
                _fixedUpdateActions.Add(action);
            }
            _toAddFixedUpdateActions.Clear();

            foreach (var updateAction in _fixedUpdateActions)
            {
                if (_clearAllActions) break;
                if (_toRemoveFixedUpdateActions.Contains(updateAction)) continue;

                updateAction?.Invoke(Time.fixedDeltaTime);
            }

            if (_clearAllActions)
            {
                ClearAllActionsComplete();
            }
        }

        public void ClearAllActions()
        {
            _clearAllActions = true;
        }

        private void ClearAllActionsComplete()
        {
            _updateActions.Clear();
            _toAddUpdateActions.Clear();
            _toRemoveUpdateActions.Clear();
            _fixedUpdateActions.Clear();
            _toAddFixedUpdateActions.Clear();
            _toRemoveFixedUpdateActions.Clear();
            _clearAllActions = false;
        }
    }
}
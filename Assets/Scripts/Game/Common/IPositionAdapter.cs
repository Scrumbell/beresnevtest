﻿using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public interface IPositionAdapter
    {
        void Set(Vector3 position);
        void SetImmediate(Vector3 position);
        void Translate(Vector3 offset);
        Vector3 Get();
    }
}

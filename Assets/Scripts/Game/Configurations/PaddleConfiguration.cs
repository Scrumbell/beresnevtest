﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Configurations
{
    [CreateAssetMenu(fileName = "PaddleConfiguration", menuName = "Configurations/Paddle")]
    public class PaddleConfiguration : Configuration
    {
        [SerializeField]
        private float Speed;
        [SerializeField]
        private float Range;

        public PaddleStats GetStats() {
            return new PaddleStats(Speed, Range);
        }
    }
}

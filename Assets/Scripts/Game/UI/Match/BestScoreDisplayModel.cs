﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Events;
using TMPro;

namespace BeresnevTest.Game.UI.Match
{
    public class BestScoreDisplayParams : IModelParams
    {
        public TextMeshProUGUI ScoreText { get; private set; }

        public BestScoreDisplayParams(TextMeshProUGUI scoreText)
        {
            ScoreText = scoreText;
        }
    }
    public class BestScoreDisplayModel : Model<BestScoreDisplayParams>
    {
        private TextMeshProUGUI _scoreText;

        protected override void Init(BestScoreDisplayParams modelParams)
        {
            base.Init(modelParams);
            _scoreText = modelParams.ScoreText;

            _scoreText.text = "0";

            EventDispatcher.AddListener<BestScoreChangedEvent>(OnBestScoreChanged);
        }

        private void OnBestScoreChanged(BestScoreChangedEvent e)
        {
            _scoreText.text = e.Score.ToString();
        }

        public override void Destroy()
        {
            EventDispatcher.RemoveListener<BestScoreChangedEvent>(OnBestScoreChanged);

            base.Destroy();
        }
    }
}

﻿using UnityEngine.Events;
using UnityEngine.EventSystems;
using BeresnevTest.Core.Mvc;

namespace BeresnevTest.Game.UI.Common
{
    public class PointerElement : View, IPointerDownHandler, IPointerUpHandler
    {
        public class OnPointerChangeEvent : UnityEvent<bool>
        {

        }

        public OnPointerChangeEvent OnPointerChanged
        {
            get { return _onPointerChanged; }
        }

        private readonly OnPointerChangeEvent _onPointerChanged = new OnPointerChangeEvent();
        private bool _pointerDown;

        public void OnPointerDown(PointerEventData eventData)
        {
            ChangePointer(true);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            ChangePointer(false);
        }

        private void OnApplicationFocus(bool focus)
        {
            if (_pointerDown)
            {
                ChangePointer(false);
            }
        }

        private void ChangePointer(bool state)
        {
            _pointerDown = state;
            _onPointerChanged.Invoke(state);
        }
    }
}
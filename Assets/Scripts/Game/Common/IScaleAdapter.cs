﻿using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public interface IScaleAdapter
    {
        void Set(float scale);
        void Set(Vector3 scale);
        Vector3 Get();
    }
}
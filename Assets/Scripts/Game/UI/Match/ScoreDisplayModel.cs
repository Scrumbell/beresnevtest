﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.Events;
using TMPro;

namespace BeresnevTest.Game.UI.Match
{
    public class ScoreDisplayParams : IModelParams { 
        public TextMeshProUGUI ScoreText { get; private set; }
        public PlayerNumber PlayerNumber { get; private set; }

        public ScoreDisplayParams(TextMeshProUGUI scoreText, PlayerNumber playerNumber) {
            ScoreText = scoreText;
            PlayerNumber = playerNumber;
        }
    }
    public class ScoreDisplayModel : Model<ScoreDisplayParams>
    {
        private TextMeshProUGUI _scoreText;
        private PlayerNumber _playerNumber;

        protected override void Init(ScoreDisplayParams modelParams)
        {
            base.Init(modelParams);
            _scoreText = modelParams.ScoreText;
            _playerNumber = modelParams.PlayerNumber;

            _scoreText.text = "0";

            EventDispatcher.AddListener<ScoreChangedEvent>(OnScoreChanged);
        }

        private void OnScoreChanged(ScoreChangedEvent e) {
            if (e.PlayerNumber == _playerNumber)
            {
                _scoreText.text = e.Score.ToString();
            }
        }

        public override void Destroy()
        {
            EventDispatcher.RemoveListener<ScoreChangedEvent>(OnScoreChanged);

            base.Destroy();
        }
    }
}

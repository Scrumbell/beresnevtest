﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public class TransformScaleParams : IModelParams
    {
        public Transform Transform { get; private set; }
        public TransformScaleParams(Transform transform)
        {
            Transform = transform;
        }
    }

    public class TransformScale : Model<TransformScaleParams>, IScaleAdapter
    {
        private Transform _transform;

        protected override void Init(TransformScaleParams modelParams)
        {
            base.Init(modelParams);
            _transform = modelParams.Transform;
        }

        public void Set(float scale) {
            Set(Vector3.one * scale);
        }

        public void Set(Vector3 scale)
        {
            _transform.localScale = scale;
        }

        public Vector3 Get()
        {
            return _transform.localScale;
        }
    }
}

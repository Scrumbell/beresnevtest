﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Constants;
using BeresnevTest.Game.Actors;
using BeresnevTest.Game.Controls;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.Events;
using UnityEngine;

namespace BeresnevTest.Game.Levels
{
    public class PongLevelParams : IModelParams {
        public Transform LevelContainer { get; private set; }
        public IAxisValueProvider Player1ValueProvider { get; private set; }
        public IAxisValueProvider Player2ValueProvider { get; private set; }

        public PongLevelParams(Transform levelContainer, IAxisValueProvider player1ValueProvider, IAxisValueProvider player2ValueProvider) {
            LevelContainer = levelContainer;
            Player1ValueProvider = player1ValueProvider;
            Player2ValueProvider = player2ValueProvider;
        }
    }
    public class PongLevelModel : Model<PongLevelParams>
    {
        private PongLevelView _view;

        private PaddleModel _player1Paddle;
        private PaddleModel _player2Paddle;
        private BallModel _ball;

        protected override void Init(PongLevelParams modelParams)
        {
            base.Init(modelParams);

            _view = CreateViewChild<PongLevelView>(modelParams.LevelContainer, ViewPaths.LEVELS);

            _player1Paddle = CreateModelChild<PaddleModel>(new PaddleParams(_view.Player1PaddleContainer, 
                modelParams.Player1ValueProvider, 
                _view.PaddleConfiguration.GetStats()));
            _player2Paddle = CreateModelChild<PaddleModel>(new PaddleParams(_view.Player2PaddleContainer, 
                modelParams.Player2ValueProvider, 
                _view.PaddleConfiguration.GetStats()));
            _ball = CreateModelChild<BallModel>(new BallParams(_view.BallContainer, _view.BallConfiguration.GetStats()));

            _view.Player1GoalWall.CollisionEnter += Player1GoalHit;
            _view.Player2GoalWall.CollisionEnter += Player2GoalHit;
        }

        public void StartNewMatch() {
            _ball.SetStats(_view.BallConfiguration.GetStats());
        }

        public void ResetField()
        {
            _player1Paddle.Reset();
            _player2Paddle.Reset();
            _ball.Reset();
        }

        public void StartGame() {
            _player1Paddle.SetPaused(false);
            _player2Paddle.SetPaused(false);
            _ball.SetPaused(false);
        }

        private void Player1GoalHit(Collider collider) {
            OnGoalHit(PlayerNumber.Player2);
        }

        private void Player2GoalHit(Collider collider)
        {
            OnGoalHit(PlayerNumber.Player1);
        }

        private void OnGoalHit(PlayerNumber scoredPlayer)
        {
            EventDispatcher.DispatchEvent(new GoalHitEvent(scoredPlayer));
        }

        public override void Destroy()
        {
            _view.Player1GoalWall.CollisionEnter -= Player1GoalHit;
            _view.Player2GoalWall.CollisionEnter -= Player2GoalHit;
            base.Destroy();
        }
    }
}

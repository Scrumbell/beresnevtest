﻿namespace BeresnevTest.Game.Controls {
    public enum AxisValue
    { 
        Negative = -1,
        None = 0,
        Positive = 1
    }
}
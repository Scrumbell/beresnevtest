﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Locators
{
    public class GameSceneLocator : View
    {
        public Transform GameLevelContainer;
        public Transform UIContainer;
        public Transform Player1ButtonsContainer;
        public Transform Player2ButtonsContainer;
    }
}

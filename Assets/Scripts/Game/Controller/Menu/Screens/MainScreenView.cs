﻿using BeresnevTest.Core.Mvc;
using UnityEngine.UI;

namespace BeresnevTest.Game.Controller.Menu.Screens
{
    public class MainScreenView : View
    {
        public Button StartButton;
        public Button SettingsButton;
    }
}

﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Common;
using BeresnevTest.Game.Configurations;
using UnityEngine;

namespace BeresnevTest.Game.Levels
{
    public class PongLevelView : View
    {
        public Transform Player1PaddleContainer;
        public Transform Player2PaddleContainer;
        public Transform BallContainer;
        public CollisionElement Player1GoalWall;
        public CollisionElement Player2GoalWall;
        public PaddleConfiguration PaddleConfiguration;
        public BallConfiguration BallConfiguration;
    }
}

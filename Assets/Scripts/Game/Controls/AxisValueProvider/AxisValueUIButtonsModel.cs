﻿using UnityEngine;
using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Constants;

namespace BeresnevTest.Game.Controls.UI
{
    public class AxisValueUiButtonsParams : IModelParams {
        public Transform UIButtonsContainer { get; private set; }

        public AxisValueUiButtonsParams(Transform uiButtonsContainer) {
            UIButtonsContainer = uiButtonsContainer;
        }

    }
    public class AxisValueUIButtonsModel : AxisValueProvider<AxisValueUiButtonsParams>
    {
        private bool _negativePressed;
        private bool _positivePressed;

        protected override void Init(AxisValueUiButtonsParams modelParams)
        {
            base.Init(modelParams);
            var view = CreateViewChild<AxisValueUIButtonsView>(modelParams.UIButtonsContainer, ViewPaths.UI_CONTROLS);
            AttachPointerListener(view.NegativeButton, NegativePointerChanged);
            AttachPointerListener(view.PositiveButton, PositivePointerChanged);
        }

        public override AxisValue GetAxisValue()
        {
            if (_negativePressed) {
                if (_positivePressed) {
                    return AxisValue.None;
                }
                return AxisValue.Negative;
            }
            else if (_positivePressed) {
                return AxisValue.Positive;
            }
            return AxisValue.None;
        }

        void NegativePointerChanged(bool value) {
            _negativePressed = value;
        }

        void PositivePointerChanged(bool value)
        {
            _positivePressed = value;
        }
    }
}

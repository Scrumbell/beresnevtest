﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Constants;
using UnityEngine;

namespace BeresnevTest.Game.Controller.Menu.Screens
{
    public interface IMenuScreenModel {
        void Activate();
        void Deactivate();
    }

    public interface IMenuScreenParams : IModelParams { 
        Transform ViewContainer { get; }
    }

    public class MenuScreenModel<TParams, TView> : Model<TParams>, IMenuScreenModel where TParams : IMenuScreenParams where TView : IView
    {
        protected TView View;

        protected override void Init(TParams modelParams)
        {
            base.Init(modelParams);

            View = CreateViewChild<TView>(modelParams.ViewContainer, ViewPaths.UI_MENU_SCREENS);
        }

        public virtual void Activate() {
            View.Show();
        }

        public virtual void Deactivate() {
            View.Hide();
        }
    }
}

﻿namespace BeresnevTest.Game.Constants
{
    public static class ViewPaths
    {
        public const string GAME_ROOT = "Game/";
        public const string ACTORS = GAME_ROOT + "Actors/";
        public const string LEVELS = GAME_ROOT + "Levels/";

        public const string UI = GAME_ROOT + "UI/";
        public const string UI_CONTROLS = UI + "Controls/";
        public const string UI_MATCH = UI + "Match/";
        public const string UI_MENU = UI + "Menu/";
        public const string UI_MENU_SCREENS = UI_MENU + "Screens/";
    }
}

﻿namespace BeresnevTest.Game.Configurations
{
    public class PaddleStats
    {
        public float Speed { get; private set; }
        public float Range { get; private set; }

        public PaddleStats(float speed, float range) {
            Speed = speed;
            Range = range;
        }
    }
}

﻿using System;
using UnityEngine;

namespace BeresnevTest.Game.Configurations
{
    [Serializable]
    public struct BallColor
    {
        [SerializeField]
        private string _name;

        [SerializeField]
        private Material _color;

        public string Name { 
            get {
                return _name;
            }
        }

        public Material Color {
            get {
                return _color;
            }
        }
    }
}

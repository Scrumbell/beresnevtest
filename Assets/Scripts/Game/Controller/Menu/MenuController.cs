﻿using System;
using System.Collections.Generic;
using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.Locators;
using BeresnevTest.Game.Controller.Menu.Events;
using BeresnevTest.Game.Controller.Menu.Screens;

namespace BeresnevTest.Game.Controller.Menu
{
    public class MenuControllerParams : IModelParams { 
        public MenuSceneLocator MenuSceneLocator { get; private set; }

        public MenuControllerParams(MenuSceneLocator menuSceneLocator) {
            MenuSceneLocator = menuSceneLocator;
        }
    }
    public class MenuController : Model<MenuControllerParams>
    {
        private MenuSceneLocator _sceneLocator;

        private Dictionary<MenuScreen, IMenuScreenModel> _menuScreens;

        protected override void Init(MenuControllerParams modelParams)
        {
            base.Init(modelParams);

            _sceneLocator = modelParams.MenuSceneLocator;

            _menuScreens = new Dictionary<MenuScreen, IMenuScreenModel>();

            EventDispatcher.AddListener<ChangeScreenEvent>(OnChangeScreen);

            SetScreen(MenuScreen.Main);
        }

        private void SetScreen(MenuScreen screen) {
            foreach (var pair in _menuScreens) {
                if (pair.Key != screen) {
                    pair.Value.Deactivate();
                }
            }

            if (_menuScreens.ContainsKey(screen))
            {
                _menuScreens[screen].Activate();
            }
            else {
                var menuScreenModel = CreateScreenModel(screen);
                menuScreenModel.Activate();
                _menuScreens[screen] = menuScreenModel;
            }
        }

        private IMenuScreenModel CreateScreenModel(MenuScreen screen) {
            switch (screen)
            {
                case MenuScreen.Main:
                    return CreateModelChild<MainScreenModel>(new MainScreenParams(_sceneLocator.ScreensContainer));
                case MenuScreen.Settings:
                    return CreateModelChild<SettingsScreenModel>(new SettingsScreenParams(_sceneLocator.ScreensContainer, _sceneLocator.BallContainer));
                default:
                    throw new Exception(this + ".CreateScreenModel: " + screen + " screen is not supported.");
            }
        }

        private void OnChangeScreen(ChangeScreenEvent e) {
            SetScreen(e.Screen);
        }

        public override void Destroy()
        {
            EventDispatcher.RemoveListener<ChangeScreenEvent>(OnChangeScreen);

            base.Destroy();
        }
    }
}

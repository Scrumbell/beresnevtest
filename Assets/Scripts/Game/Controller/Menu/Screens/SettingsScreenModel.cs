﻿using BeresnevTest.Game.Data;
using BeresnevTest.Game.Actors;
using BeresnevTest.Game.Configurations;
using BeresnevTest.Game.Controller.Menu.Events;
using UnityEngine;

namespace BeresnevTest.Game.Controller.Menu.Screens
{
    public class SettingsScreenParams : IMenuScreenParams { 
        public Transform ViewContainer { get; private set; }
        public Transform BallContainer { get; private set; }

        public SettingsScreenParams(Transform viewContainer, Transform ballContainer) {
            ViewContainer = viewContainer;
            BallContainer = ballContainer;
        }
    }

    public class SettingsScreenModel : MenuScreenModel<SettingsScreenParams, SettingsScreenView>
    {
        private BallVisualsModel _ball;

        private BallColorsConfiguration _ballColorsConfig;

        private int _currentColorIndex;

        protected override void Init(SettingsScreenParams modelParams)
        {
            base.Init(modelParams);

            _ballColorsConfig = ObjectCreator.LoadConfiguration<BallColorsConfiguration>();

            _ball = CreateModelChild<BallVisualsModel>(new BallVisualsParams(modelParams.BallContainer, _ballColorsConfig.CurrentColor));

            AttachButtonListener(View.LeftButton, OnLeftButton);
            AttachButtonListener(View.RightButton, OnRightButton);
            AttachButtonListener(View.ApplyButton, OnApplyButton);
            AttachButtonListener(View.BackButton, OnBackButton);

            ChangeColor(_ballColorsConfig.CurrentColorIndex);
        }

        public override void Activate()
        {
            base.Activate();
            _ball.Show();
        }

        public override void Deactivate()
        {
            _ball.Hide();
            base.Deactivate();
        }

        private void ChangeColor(int colorIndex) {
            _currentColorIndex = colorIndex;
            BallColor color = _ballColorsConfig.GetColor(_currentColorIndex);
            _ball.SetColor(color);
            View.ColorNameText.text = color.Name;
            View.ApplyButton.gameObject.SetActive(_currentColorIndex != _ballColorsConfig.CurrentColorIndex);
        }

        private void OnLeftButton()
        {
            ChangeColor(_currentColorIndex == 0 ? _ballColorsConfig.ColorsCount - 1 : _currentColorIndex - 1);
        }

        private void OnRightButton()
        {
            ChangeColor(_currentColorIndex == _ballColorsConfig.ColorsCount - 1 ? 0 : _currentColorIndex + 1);
        }

        private void OnApplyButton() {
            _ballColorsConfig.SetCurrentColor(_currentColorIndex);
            View.ApplyButton.gameObject.SetActive(_currentColorIndex != _ballColorsConfig.CurrentColorIndex);
        }

        private void OnBackButton()
        {
            EventDispatcher.DispatchEvent(new ChangeScreenEvent(MenuScreen.Main));
        }
    }
}

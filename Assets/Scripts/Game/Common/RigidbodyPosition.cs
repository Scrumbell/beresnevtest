﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public class RigidbodyPositionParams : IModelParams {
        public Rigidbody Rigidbody { get; private set; }
        public RigidbodyPositionParams(Rigidbody rigidbody) {
            Rigidbody = rigidbody;
        }
    }

    public class RigidbodyPosition : Model<RigidbodyPositionParams>, IPositionAdapter
    {
        private Rigidbody _rigidbody;

        protected override void Init(RigidbodyPositionParams modelParams)
        {
            base.Init(modelParams);
            _rigidbody = modelParams.Rigidbody;
        }

        public void Set(Vector3 position) {
            _rigidbody.MovePosition(_rigidbody.transform.parent.TransformPoint(position));
        }

        public void SetImmediate(Vector3 position)
        {
            _rigidbody.transform.localPosition = position;
        }

        public void Translate(Vector3 offset) {
            _rigidbody.MovePosition(_rigidbody.position + _rigidbody.transform.TransformDirection(offset));
        }

        public Vector3 Get() {
            return _rigidbody.transform.localPosition;
        }
    }
}

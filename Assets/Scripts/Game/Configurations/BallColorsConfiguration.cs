﻿using System;
using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Constants;
using UnityEngine;

namespace BeresnevTest.Game.Configurations
{
    [CreateAssetMenu(fileName = "BallColorsConfiguration", menuName = "Configurations/BallColors")]
    public class BallColorsConfiguration : Configuration
    {
        [SerializeField]
        private BallColor[] BallColors;
        [SerializeField]
        private BallColor FallbackColor;

        public int ColorsCount { 
            get {
                return BallColors.Length;
            }
        }

        public BallColor CurrentColor
        {
            get
            {
                return GetColor(CurrentColorIndex);
            }
        }

        public int CurrentColorIndex {
            get {
                return PlayerPrefs.GetInt(PlayerPrefsConstants.BALL_COLOR, 0);
            }
        }

        public void SetCurrentColor(BallColor color) {
            int index = Array.IndexOf(BallColors, color);
            if (index >= 0)
            {
                PlayerPrefs.SetInt(PlayerPrefsConstants.BALL_COLOR, index);
            }
            else {
                Debug.LogWarning(this + ".SetCurrentColor: configuration does not contain a given color, current color was not set.");
            }
        }

        public void SetCurrentColor(int colorIndex)
        {
            if (colorIndex < BallColors.Length)
            {
                PlayerPrefs.SetInt(PlayerPrefsConstants.BALL_COLOR, colorIndex);
            }
            else
            {
                Debug.LogWarning(this + ".SetCurrentColor: configuration does not contain a given color, current color was not set.");
            }
        }

        public BallColor GetColor(int colorIndex) {
            if (colorIndex < 0 || colorIndex >= BallColors.Length) {
                Debug.LogWarning(this + ".GetColor: index " + colorIndex + " is out of range, using fallback color");
                return FallbackColor;
            }
            else{
                return BallColors[colorIndex];
            }
        }
    }
}
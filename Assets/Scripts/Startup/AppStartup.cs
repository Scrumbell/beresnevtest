﻿using BeresnevTest.Core.AppState;
using BeresnevTest.Core.Injection;
using BeresnevTest.Core.EventSystem;
using BeresnevTest.Core.Mvc;
using BeresnevTest.Core.SceneLoading;
using BeresnevTest.Core.UpdateManagement;
using BeresnevTest.Game.States;
using UnityEngine;

namespace BeresnevTest.Startup
{
    public class AppStartup : MonoBehaviour
    {
        public UpdateManager UpdateManager;

        private Injector _injector;
        private EventDispatcher _eventDispatcher;
        private AppStateMachine _appStateMachine;
        private SceneLoader _sceneLoader;

        void Start()
        {
            _injector = new Injector();
            _eventDispatcher = new EventDispatcher();
            _sceneLoader = new SceneLoader();
            _sceneLoader.Init();
            _appStateMachine = new AppStateMachine();

            InjectorMapping();

            _appStateMachine.SetState(new MenuState());
        }

        private void InjectorMapping()
        {
            _injector.Map<IInjector>(_injector);
            _injector.Map<IEventDispatcher>(_eventDispatcher);
            _injector.Map<IAppStateMachine>(_appStateMachine);
            _injector.Map<IObjectCreator>(new ObjectCreator());
            _injector.Map<ISceneLoader>(_sceneLoader);
            _injector.Map<IUpdateManager>(UpdateManager);
            _injector.PostMapping();
        }

        private void OnDestroy()
        {
            UpdateManager.ClearAllActions();
            _appStateMachine.Destroy();
            _sceneLoader.Destroy();
            _eventDispatcher.RemoveAllEvents();
            _injector.Destroy();
        }

    }
}

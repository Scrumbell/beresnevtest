﻿using BeresnevTest.Core.EventSystem.Events;
using BeresnevTest.Game.Data;

namespace BeresnevTest.Game.Events
{
    public class ScoreChangedEvent : EventBase
    {
        public int Score { get; private set; }
        public PlayerNumber PlayerNumber { get; private set; }

        public ScoreChangedEvent(int score, PlayerNumber playerNumber) {
            Score = score;
            PlayerNumber = playerNumber;
        }
    }
}

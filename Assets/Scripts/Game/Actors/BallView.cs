﻿using BeresnevTest.Game.Common;
using UnityEngine;

namespace BeresnevTest.Game.Actors
{
    public class BallView : PhysicsActorView
    {
        public Transform BallVisualsContainer;
        public CollisionElement CollisionElement;
    }
}

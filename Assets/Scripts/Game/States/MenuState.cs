﻿using BeresnevTest.Utils.Scenes;
using BeresnevTest.Core.SceneLoading;
using BeresnevTest.Core.AppState;
using BeresnevTest.Game.Locators;
using BeresnevTest.Game.Controller.Menu;

namespace BeresnevTest.Game.States
{
    public class MenuState : AppState
    {
        private MenuController _menuController;
        public override void TransitionIn()
        {
            SceneLoader.LoadScene(SceneConstants.MENU_SCENE, (scene) => {
                MenuSceneLocator menuSceneLocator = SceneManagerUtils.GetComponentInRoot<MenuSceneLocator>(scene);
                _menuController = ObjectCreator.CreateModel<MenuController>(new MenuControllerParams(menuSceneLocator));

                TransitionInFinished();
            });
        }
        public override void TransitionOut()
        {
            _menuController.Destroy();
            SceneLoader.UnloadScene(SceneConstants.MENU_SCENE, TransitionOutFinished);
        }
    }
}

﻿using BeresnevTest.Core.Injection;
using BeresnevTest.Core.UpdateManagement;
using BeresnevTest.Game.Controls;
using BeresnevTest.Game.Configurations;
using UnityEngine;

namespace BeresnevTest.Game.Actors
{
    public class PaddleParams : IPhysicsActorParams
    {
        public Transform ViewContainer { get; private set; }
        public IAxisValueProvider ValueProvider { get; private set; }
        public PaddleStats Stats { get; private set; }

        public PaddleParams(Transform viewContainer, IAxisValueProvider valueProvider, PaddleStats stats) {
            ViewContainer = viewContainer;
            ValueProvider = valueProvider;
            Stats = stats;
        }
    }
    public class PaddleModel : PhysicsActor<PaddleParams, PaddleView>
    {
        private IAxisValueProvider _valueProvider;

        private PaddleStats _stats;

        protected override void Init(PaddleParams modelParams)
        {
            base.Init(modelParams);
            Reset();

            _stats = modelParams.Stats;
            _valueProvider = modelParams.ValueProvider;
        }

        protected override void UpdateActorImpl(float deltaTime)
        {
            float axisValue = _valueProvider.GetAxisValueFloat();
            if (axisValue != 0f)
            {
                Vector3 currentPos = _positionAdapter.Get();
                _positionAdapter.Set(new Vector3(Mathf.Clamp(currentPos.x + _stats.Speed * deltaTime * axisValue, -_stats.Range, _stats.Range),
                    currentPos.y, currentPos.z));
            }
        }
    }
}

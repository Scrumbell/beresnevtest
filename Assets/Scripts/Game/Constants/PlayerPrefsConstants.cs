﻿namespace BeresnevTest.Game.Constants
{
    public static class PlayerPrefsConstants
    {
        public const string BEST_SCORE = "bestScore";
        public const string BALL_COLOR = "ballColor";
    }
}

﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Constants;
using BeresnevTest.Game.Configurations;
using UnityEngine;

namespace BeresnevTest.Game.Actors
{
    public class BallVisualsParams : IModelParams
    {
        public Transform ViewContainer { get; private set; }
        public BallColor Color { get; private set; }
        public BallVisualsParams(Transform viewContainer, BallColor color)
        {
            ViewContainer = viewContainer;
            Color = color;
        }
    }
    public class BallVisualsModel : Model<BallVisualsParams>
    {
        private BallVisualsView _view;
        protected override void Init(BallVisualsParams modelParams)
        {
            base.Init(modelParams);

            _view = CreateViewChild<BallVisualsView>(modelParams.ViewContainer, ViewPaths.ACTORS);

            SetColor(modelParams.Color);
        }

        public void SetColor(BallColor color)
        {
            _view.Renderer.material = color.Color;
        }

        public void Show() {
            _view.Show();
        }

        public void Hide() {
            _view.Hide();
        }
    }
}

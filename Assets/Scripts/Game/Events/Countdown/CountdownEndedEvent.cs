﻿namespace BeresnevTest.Game.Events
{
    public class CountdownEndedEvent : CountdownEvent
    {
        public CountdownEndedEvent(float countdownTime) : base(countdownTime) { }
    }
}
﻿using BeresnevTest.Core.Mvc;
using UnityEngine.UI;
using TMPro;

namespace BeresnevTest.Game.UI.Match
{
    public class MatchUIView : View
    {
        public TextMeshProUGUI TimerText;
        public TextMeshProUGUI Player1ScoreText;
        public TextMeshProUGUI Player2ScoreText;
        public TextMeshProUGUI BestScoreText;
        public Button ResetButton;
        public Button MenuButton;
    }
}

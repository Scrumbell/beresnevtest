﻿using BeresnevTest.Core.Injection;
using UnityEngine;

namespace BeresnevTest.Core.Mvc
{
    public interface IObjectCreator {
        T CreateModel<T>(IModelParams modelParams) where T : IModel, new();
        T CreateView<T>(Transform parent, string path = "") where T : IView;
        T LoadConfiguration<T>(string customPath = "", string customName = null) where T : Object, IConfiguration;
    }

    public class ObjectCreatorException : System.Exception {
        public ObjectCreatorException(string message) : base(message)
        {
        }
    }

    public class ObjectCreator : IObjectCreator
    {
        public const string PREFABS_ROOT = "Prefabs/";
        public const string CONFIGURATIONS_ROOT = "Configurations/";

        [Inject]
        private IInjector _injector;

        public T CreateModel<T>(IModelParams modelParams) where T : IModel, new() 
        {
            var model = new T();
            _injector.InjectInto(model);
            model.Init(modelParams);

            return model;
        }

        public T CreateView<T>(Transform parent, string customPath = "") where T : IView
        {
            string typeName = typeof(T).Name;

            if (parent == null) {
                throw new ObjectCreatorException(typeName + " view cannot have null parent");
            }

            string path = PREFABS_ROOT + customPath + typeName;

            GameObject loadedGO = Resources.Load<GameObject>(path);
            if (loadedGO == null) {
                throw new ObjectCreatorException(typeName + " view was not found at path: " + path);
            }

            GameObject viewInstance = Object.Instantiate(loadedGO, parent, false);
            T result = viewInstance.GetComponent<T>();
            if (result == null) {
                Object.Destroy(viewInstance);
                throw new ObjectCreatorException(viewInstance.ToString() + " does not contain component of type " + typeName);
            }

            return result;
        }
        public T LoadConfiguration<T>(string customPath = "", string customName = null) where T : Object, IConfiguration
        {
            string typeName = typeof(T).Name;
            string name = customName == null ? typeName : customName;
            string path = CONFIGURATIONS_ROOT + customName + name;

            T configuration = Resources.Load<T>(path);
            if (configuration == null)
            {
                throw new ObjectCreatorException(typeName + " configuration was not found at path: " + path);
            }
            return configuration;
        }
    }
}

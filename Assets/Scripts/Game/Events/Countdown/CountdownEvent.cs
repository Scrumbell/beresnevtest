﻿using BeresnevTest.Core.EventSystem.Events;

namespace BeresnevTest.Game.Events
{
    public class CountdownEvent : EventBase
    {
        public float CountdownTime { get; private set; }

        public CountdownEvent(float countdownTime) {
            CountdownTime = countdownTime;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public class CollisionListenerParams : IModelParams {
        public ICollisionElement CollisionElement;

        public CollisionListenerParams(ICollisionElement CollisionElement) {
            this.CollisionElement = CollisionElement;
        }
    }
    //TODO: Might need to add <Collider> to Action
    public class CollisionListener : Model<CollisionListenerParams>
    {
        private ICollisionElement _collisionElement;
        private Dictionary<string, Action> _tagListeners;

        protected override void Init(CollisionListenerParams modelParams)
        {
            base.Init(modelParams);
            _collisionElement = modelParams.CollisionElement;
            _tagListeners = new Dictionary<string, Action>();

            _collisionElement.CollisionEnter += OnCollisionEnter;
        }

        public void AddTagListener(string tag, Action action) {
            if (_tagListeners.ContainsKey(tag))
            {
                Debug.LogWarning(this + ".AddTagListener: listener is already listening tag " + tag);
            }
            else {
                _tagListeners.Add(tag, action);
            }
        }

        public void RemoveTagListener(string tag) {
            if (_tagListeners.ContainsKey(tag))
            {
                _tagListeners.Remove(tag);
            }
            else {
                Debug.LogWarning(this + ".RemoveTagListener: listener is not listening for the tag " + tag);
            }
        }

        private void OnCollisionEnter(Collider collidedObj) {
            foreach (var pair in _tagListeners) {
                if (collidedObj.CompareTag(pair.Key)) {
                    pair.Value?.Invoke();
                }
            }
        }

        public override void Destroy()
        {
            _tagListeners.Clear();
            _collisionElement.CollisionEnter -= OnCollisionEnter;
            base.Destroy();
        }
    }
}

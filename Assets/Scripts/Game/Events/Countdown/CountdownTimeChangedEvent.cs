﻿namespace BeresnevTest.Game.Events
{
    public class CountdownTimeChangedEvent : CountdownEvent
    {
        public CountdownTimeChangedEvent(float countdownTime) : base(countdownTime) { }
    }
}
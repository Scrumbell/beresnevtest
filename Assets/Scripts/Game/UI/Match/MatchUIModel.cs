﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Core.Injection;
using BeresnevTest.Core.AppState;
using BeresnevTest.Game.States;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.Constants;
using BeresnevTest.Game.UI.Match.Events;
using UnityEngine;

namespace BeresnevTest.Game.UI.Match
{
    public class MatchUIParams : IModelParams { 
        public Transform ViewContainer { get; private set; }
        public MatchUIParams(Transform viewContainer) {
            ViewContainer = viewContainer;
        }
    }
    public class MatchUIModel : Model<MatchUIParams>
    {
        [Inject]
        private IAppStateMachine _appStateMachine;

        private MatchUIView _view;
        protected override void Init(MatchUIParams modelParams)
        {
            base.Init(modelParams);
            _view = CreateViewChild<MatchUIView>(modelParams.ViewContainer, ViewPaths.UI_MATCH);

            CreateModelChild<CountdownTimerModel>(new CountdownTimerParams(_view.TimerText));
            CreateModelChild<ScoreDisplayModel>(new ScoreDisplayParams(_view.Player1ScoreText, PlayerNumber.Player1));
            CreateModelChild<ScoreDisplayModel>(new ScoreDisplayParams(_view.Player2ScoreText, PlayerNumber.Player2));
            CreateModelChild<BestScoreDisplayModel>(new BestScoreDisplayParams(_view.BestScoreText));

            AttachButtonListener(_view.MenuButton, OnMenuButton);
            AttachButtonListener(_view.ResetButton, OnResetButton);
        }

        public void OnMenuButton() { 
            _appStateMachine.SetState(new MenuState());
        }

        public void OnResetButton() {
            EventDispatcher.DispatchEvent(new ResetMatchEvent());
        }

        public override void Destroy()
        {

            base.Destroy();
        }
    }
}

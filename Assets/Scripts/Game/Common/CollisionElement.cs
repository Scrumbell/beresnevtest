﻿using System;
using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Common
{
    public interface ICollisionElement { 
        Action<Collider> CollisionEnter { get; set; }
    }
    public class CollisionElement : View, ICollisionElement
    {
        public Action<Collider> CollisionEnter { get; set; }

        private void OnTriggerEnter(Collider other)
        {
            CollisionEnter?.Invoke(other);
        }

        private void OnCollisionEnter(Collision collision)
        {
            CollisionEnter?.Invoke(collision.collider);
        }
    }
}

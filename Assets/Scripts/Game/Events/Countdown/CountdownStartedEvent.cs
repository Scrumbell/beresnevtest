﻿namespace BeresnevTest.Game.Events
{
    public class CountdownStartedEvent : CountdownEvent
    {
        public CountdownStartedEvent(float countdownTime) : base(countdownTime) { }
    }
}

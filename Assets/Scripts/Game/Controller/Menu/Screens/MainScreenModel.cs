﻿using BeresnevTest.Core.Injection;
using BeresnevTest.Core.AppState;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.States;
using BeresnevTest.Game.Controller.Menu.Events;
using UnityEngine;

namespace BeresnevTest.Game.Controller.Menu.Screens
{
    public class MainScreenParams : IMenuScreenParams { 
        public Transform ViewContainer { get; private set; }

        public MainScreenParams(Transform viewContainer) {
            ViewContainer = viewContainer;
        }
    }

    public class MainScreenModel : MenuScreenModel<MainScreenParams, MainScreenView>
    {
        [Inject]
        private IAppStateMachine _appStateMachine;

        protected override void Init(MainScreenParams modelParams)
        {
            base.Init(modelParams);

            AttachButtonListener(View.StartButton, OnStartButton);
            AttachButtonListener(View.SettingsButton, OnSettingsButton);
        }

        private void OnStartButton() {
            _appStateMachine.SetState(new GameState());
        }

        private void OnSettingsButton() {
            EventDispatcher.DispatchEvent(new ChangeScreenEvent(MenuScreen.Settings));
        }
    }
}

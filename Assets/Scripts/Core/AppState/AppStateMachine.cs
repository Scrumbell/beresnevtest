﻿using BeresnevTest.Core.Injection;
using System.Collections.Generic;

namespace BeresnevTest.Core.AppState
{
    public enum TransitionType
    {
        None,
        TransitionIn,
        TransitionOut
    }

    public interface IAppStateMachine {
        void SetState(AppState state);
        void Destroy();
    }

    public class AppStateMachine : IAppStateMachine
    {
        [Inject]
        private IInjector _injector;

        private Queue<AppState> _nextStates;
        private AppState _currentState;
        private TransitionType _currentTransition;

        public AppStateMachine()
        {
            _nextStates = new Queue<AppState>();
            _currentTransition = TransitionType.None;
        }

        public void SetState(AppState state) {

            _injector.InjectInto(state);

            if (_currentTransition == TransitionType.None)
            {
                if (_currentState == null)
                {
                    StartState(state);
                }
                else
                {
                    _nextStates.Enqueue(state);
                    CloseCurrentState();
                }
            }
            else
            {
                _nextStates.Enqueue(state);
            }
        }

        internal void SetTransitionInFinished()
        {
            _currentTransition = TransitionType.None;


            if (_nextStates.Count > 0)
            {
                CloseCurrentState();
            }
        }

		internal void SetTransitionOutFinished()
        {
            _currentState = null;
            _currentTransition = TransitionType.None;

            if (_nextStates.Count == 0) return;

            var nextState = _nextStates.Dequeue();
            StartState(nextState);
        }


        protected virtual void StartState(AppState state)
        {
            _currentState = state;
            _currentState.Init(this);

            _currentTransition = TransitionType.TransitionIn;

            state.TransitionIn();
        }

        private void CloseCurrentState()
        {
            _currentTransition = TransitionType.TransitionOut;

            _currentState.TransitionOut();
        }

        public void Destroy()
        {
            _nextStates.Clear();
            if (_currentState != null)
            {
                CloseCurrentState();
            }
        }
    }
}

﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Core.Injection;
using BeresnevTest.Core.UpdateManagement;
using BeresnevTest.Game.Common;
using BeresnevTest.Game.Constants;
using UnityEngine;

namespace BeresnevTest.Game.Actors
{
    public interface IPhysicsActorParams : IModelParams {
        Transform ViewContainer { get; }
    }

    public interface IPhysicsActor {
        void SetPaused(bool paused);
        void Reset();
    }

    public class PhysicsActor<TParams, TView> : Model<TParams>, IPhysicsActor where TParams : IPhysicsActorParams where TView : PhysicsActorView
    {
        [Inject]
        protected IUpdateManager _updateManager;

        protected TView _view;
        protected IPositionAdapter _positionAdapter;

        protected bool _paused;

        protected override void Init(TParams modelParams)
        {
            base.Init(modelParams);

            _view = CreateViewChild<TView>(modelParams.ViewContainer, ViewPaths.ACTORS);
            _positionAdapter = CreateModelChild<RigidbodyPosition>(new RigidbodyPositionParams(_view.Rigidbody));

            _updateManager.AddFixedUpdateAction(UpdateActor);
        }

        public virtual void SetPaused(bool paused)
        {
            _paused = paused;
        }

        public virtual void Reset()
        {
            SetPaused(true);
            _positionAdapter.SetImmediate(Vector3.zero);
        }

        private void UpdateActor(float deltaTime)
        {
            if (!_paused)
            {
                UpdateActorImpl(deltaTime);
            }
        }

        protected virtual void UpdateActorImpl(float deltaTime) { }

        public override void Destroy()
        {
            _updateManager.RemoveFixedUpdateAction(UpdateActor);
            base.Destroy();
        }
    }
}

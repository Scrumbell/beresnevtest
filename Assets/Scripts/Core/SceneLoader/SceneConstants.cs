﻿namespace BeresnevTest.Core.SceneLoading
{
    public static class SceneConstants
    {
        public const string GAME_SCENE = "GameScene";
        public const string MENU_SCENE = "MenuScene";
    }
}

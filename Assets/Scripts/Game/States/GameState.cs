﻿using BeresnevTest.Utils.Scenes;
using BeresnevTest.Core.SceneLoading;
using BeresnevTest.Core.AppState;
using BeresnevTest.Game.Locators;
using BeresnevTest.Game.Controller;

namespace BeresnevTest.Game.States
{
    public class GameState : AppState
    {
        private PongGameController _pongGameController;
        public override void TransitionIn()
        {
            SceneLoader.LoadScene(SceneConstants.GAME_SCENE, (scene) => {
                GameSceneLocator gameSceneLocator = SceneManagerUtils.GetComponentInRoot<GameSceneLocator>(scene);
                _pongGameController = ObjectCreator.CreateModel<PongGameController>(new PongGameControllerParams(gameSceneLocator));

                TransitionInFinished();
            });
        }
        public override void TransitionOut()
        {
            _pongGameController.Destroy();
            SceneLoader.UnloadScene(SceneConstants.GAME_SCENE, TransitionOutFinished);
        }
    }
}

﻿using BeresnevTest.Core.EventSystem.Events;

namespace BeresnevTest.Game.Events
{
    public class BestScoreChangedEvent : EventBase
    {
        public int Score { get; private set; }

        public BestScoreChangedEvent(int score)
        {
            Score = score;
        }
    }
}

﻿using BeresnevTest.Core.EventSystem.Events;
using BeresnevTest.Game.Data;

namespace BeresnevTest.Game.Controller.Menu.Events
{
    public class ChangeScreenEvent : EventBase
    {
        public MenuScreen Screen { get; private set; }

        public ChangeScreenEvent(MenuScreen screen)
        {
            Screen = screen;
        }
    }
}

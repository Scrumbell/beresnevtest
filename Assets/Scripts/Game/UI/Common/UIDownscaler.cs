﻿using UnityEngine;

namespace BeresnevTest.Game.UI.Common
{
    [ExecuteInEditMode]
    public class UIDownscaler : MonoBehaviour
    {
        public bool ScaleInX = true;
        public bool ScaleInY = true;

        void Awake()
        {
            SetupScale();
        }

        void SetupScale()
        {
            var mainRatio = 1280f / 720f; // 1.77777777
            var currentRatio = Screen.height / (float)Screen.width;

            if (currentRatio < mainRatio)
            {
                var scale = currentRatio / mainRatio;
                transform.localScale = new Vector3(ScaleInX ? scale : 1, ScaleInY ? scale : 1, 1);
            }
            else
            {
                transform.localScale = Vector3.one;
            }
        }

#if UNITY_EDITOR
        void Update()
        {
            SetupScale();
        }
#endif
    }
}

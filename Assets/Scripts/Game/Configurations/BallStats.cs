﻿namespace BeresnevTest.Game.Configurations
{
    public class BallStats
    {
        public float Speed { get; private set; }
        public float Size { get; private set; }

        public BallStats(float speed, float size) {
            Speed = speed;
            Size = size;
        }
    }
}

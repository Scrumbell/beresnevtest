﻿using System;
using System.Collections.Generic;
using BeresnevTest.Core.Injection;
using BeresnevTest.Core.EventSystem;
using BeresnevTest.Game.UI.Common;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BeresnevTest.Core.Mvc
{
    public interface IModelParams { 
        
    }

    public class ModelParamsNone : IModelParams { 
    
    }

    public class ModelException : Exception {
        public ModelException(string message) : base(message) { }
    }

    public interface IModel
    {
        void Destroy();
        void Init(IModelParams initParams);
    }

    public class Model<TParams> : IModel where TParams : IModelParams
    {
        [Inject]
        protected IEventDispatcher EventDispatcher;

        [Inject]
        protected IObjectCreator ObjectCreator;

        private bool _inited = false;
        private bool _destroyed = false;

        private List<IModel> _createdModels;
        private List<IView> _createdViews;
        private Dictionary<Button, UnityAction> _buttonHandlers;
        private Dictionary<PointerElement, UnityAction<bool>> _pointerHandlers;

        public Model()
        {
            _createdModels = new List<IModel>();
            _createdViews = new List<IView>();
            _buttonHandlers = new Dictionary<Button, UnityAction>();
            _pointerHandlers = new Dictionary<PointerElement, UnityAction<bool>>();
        }

        public void Init(IModelParams initParams)
        {
            if (initParams == null) {
                throw new ModelException("Model init params cannot be null. Use ModelParamsNone if you don't need any parameters.");
            }
            if (!(initParams is TParams)){
                throw new ModelException("Model init params are of incorrect type. Expected type: <" 
                    + typeof(TParams).Name + ">. Provided type <" + initParams.GetType().Name + ">");
            }
            Init((TParams)initParams);
        }

        protected virtual void Init(TParams modelParams)
        {
            if (_inited)
            {
                return;
            }
            _inited = true;
        }

        protected T CreateModelChild<T>(IModelParams childParams = null) where T : IModel, new() {
            T model = ObjectCreator.CreateModel<T>(childParams);
            _createdModels.Add(model);
            return model;
        }

        protected T CreateViewChild<T>(Transform parent, string customPath = "") where T : IView {
            T view = ObjectCreator.CreateView<T>(parent, customPath);
            _createdViews.Add(view);
            return view;
        }

        protected void AttachButtonListener(Button button, Action func)
        {
            UnityAction unityAction = () => { func(); };

            button.onClick.RemoveAllListeners();
            button.onClick.AddListener(unityAction);
            _buttonHandlers[button] = unityAction;
        }

        protected void AttachPointerListener(PointerElement pointerElement, Action<bool> func)
        {
            UnityAction<bool> unityAction = (val) => { func(val); };

            pointerElement.OnPointerChanged.RemoveAllListeners();
            pointerElement.OnPointerChanged.AddListener(unityAction);
            _pointerHandlers[pointerElement] = unityAction;
        }

        private void RemoveListeners()
        {
            RemoveButtonListeners();
            RemovePointerListeners();
        }

        private void RemoveButtonListeners()
        {
            foreach (var pair in _buttonHandlers)
            {
                if (pair.Key != null)
                {
                    pair.Key.onClick.RemoveListener(pair.Value);
                }
            }
            _buttonHandlers.Clear();
        }

        private void RemovePointerListeners()
        {
            foreach (var pair in _pointerHandlers)
            {
                if (pair.Key != null)
                {
                    pair.Key.OnPointerChanged.RemoveListener(pair.Value);
                }
            }
            _pointerHandlers.Clear();
        }

        private void DestroyCreatedModels()
        {
            foreach (IModel model in _createdModels)
            {
                model.Destroy();
            }
        }

        private void DestroyCreatedViews()
        {
            foreach (IView view in _createdViews)
            {
                view.Destroy();
            }
        }

        public virtual void Destroy() {
            if (_destroyed) {
                return;
            }

            RemoveListeners();
            DestroyCreatedModels();
            DestroyCreatedViews();

            _destroyed = true;
        }
    }
}

﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Core.Injection;
using BeresnevTest.Core.UpdateManagement;
using BeresnevTest.Game.Locators;
using BeresnevTest.Game.Controls.UI;
using BeresnevTest.Game.Levels;
using BeresnevTest.Game.Events;
using BeresnevTest.Game.UI.Match;
using BeresnevTest.Game.UI.Match.Events;
using UnityEngine;

namespace BeresnevTest.Game.Controller
{
    public class PongGameControllerParams : IModelParams { 
        public GameSceneLocator GameSceneLocator { get; private set; }

        public PongGameControllerParams(GameSceneLocator gameSceneLocator) {
            GameSceneLocator = gameSceneLocator;
        }
    }
    public class PongGameController : Model<PongGameControllerParams>
    {
        private const float COUNTDOWN_TIME = 3f;

        [Inject] IUpdateManager _updateManager;

        private PongLevelModel _level;
        private ScoreModel _score;

        private float _countdownTimeLeft = 0f;

        protected override void Init(PongGameControllerParams modelParams)
        {
            base.Init(modelParams);

            CreateModelChild<MatchUIModel>(new MatchUIParams(modelParams.GameSceneLocator.UIContainer));
            var player1ValueProvider = CreateModelChild<AxisValueUIButtonsModel>(new AxisValueUiButtonsParams(modelParams.GameSceneLocator.Player1ButtonsContainer));
            var player2ValueProvider = CreateModelChild<AxisValueUIButtonsModel>(new AxisValueUiButtonsParams(modelParams.GameSceneLocator.Player2ButtonsContainer));
            _level = CreateModelChild<PongLevelModel>(new PongLevelParams(modelParams.GameSceneLocator.GameLevelContainer, player1ValueProvider, player2ValueProvider));
            _score = CreateModelChild<ScoreModel>(new ModelParamsNone());

            _updateManager.AddUpdateAction(UpdateCountdownTimer);
            EventDispatcher.AddListener<GoalHitEvent>(OnGoalHit);
            EventDispatcher.AddListener<ResetMatchEvent>(OnResetMatch);

            StartNewMatch();
        }

        private void StartNewMatch() {
            _score.ResetScore();
            _level.StartNewMatch();
            ResetField();
        }

        private void ResetField()
        {
            _level.ResetField();
            StartCountdownTimer(COUNTDOWN_TIME);
        }

        private void StartGame()
        {
            _level.StartGame();
        }

        private void StartCountdownTimer(float countdownTime) {
            _countdownTimeLeft = countdownTime;
            EventDispatcher.DispatchEvent(new CountdownStartedEvent(_countdownTimeLeft));
            EventDispatcher.DispatchEvent(new CountdownTimeChangedEvent(_countdownTimeLeft));
        }

        private void UpdateCountdownTimer(float deltaTime) {
            if (_countdownTimeLeft > 0f) {
                _countdownTimeLeft = Mathf.Max(0f, _countdownTimeLeft - deltaTime);
                EventDispatcher.DispatchEvent(new CountdownTimeChangedEvent(_countdownTimeLeft));
                if (_countdownTimeLeft <= 0f) {
                    EventDispatcher.DispatchEvent(new CountdownEndedEvent(_countdownTimeLeft));
                    StartGame();
                }
            }
        }

        private void OnGoalHit(GoalHitEvent e)
        {
            _score.AddScore(e.ScoredPlayer);
            ResetField();
        }

        private void OnResetMatch(ResetMatchEvent e) {
            StartNewMatch();
        }

        public override void Destroy()
        {
            _updateManager.RemoveUpdateAction(UpdateCountdownTimer);
            EventDispatcher.RemoveListener<GoalHitEvent>(OnGoalHit);
            EventDispatcher.RemoveListener<ResetMatchEvent>(OnResetMatch);

            base.Destroy();
        }
    }
}

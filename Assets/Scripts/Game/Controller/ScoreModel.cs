﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Data;
using BeresnevTest.Game.Events;
using BeresnevTest.Game.Constants;
using UnityEngine;

namespace BeresnevTest.Game.Controller
{
    public class ScoreModel : Model<ModelParamsNone>
    {
        private int[] _scores;
        private int _bestScore = -1;

        protected override void Init(ModelParamsNone modelParams)
        {
            base.Init(modelParams);
            _scores = new int[2];
            SetBestScore(PlayerPrefs.GetInt(PlayerPrefsConstants.BEST_SCORE, 0));
            ResetScore();
        }

        public void ResetScore() {
            SetScore(PlayerNumber.Player1, 0);
            SetScore(PlayerNumber.Player2, 0);
        }

        public void AddScore(PlayerNumber player) {
            SetScore(player, _scores[(int)player] + 1);
        }

        private void SetScore(PlayerNumber player, int score) {
            _scores[(int)player] = score;
            EventDispatcher.DispatchEvent(new ScoreChangedEvent(score, player));
            SetBestScore(score);
        }
        private void SetBestScore(int score)
        {
            if (score > _bestScore)
            {
                _bestScore = score;
                PlayerPrefs.SetInt(PlayerPrefsConstants.BEST_SCORE, _bestScore);
                EventDispatcher.DispatchEvent(new BestScoreChangedEvent(_bestScore));
            }
        }
    }
}

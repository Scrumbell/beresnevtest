﻿using System.Linq;
using BeresnevTest.Core.Mvc;
using UnityEngine.SceneManagement;

namespace BeresnevTest.Utils.Scenes
{
    public class SceneUtilsException : System.Exception {
        public SceneUtilsException(string message) : base(message) { 
        }
    }
    static public class SceneManagerUtils
    {
        public static T GetComponentInRoot<T>(Scene scene) where T : IView {
            var rootGOs = scene.GetRootGameObjects();
            T result = default;
            if (!rootGOs.Any(go => go.TryGetComponent(out result))) {
                throw new SceneUtilsException("There was no " + typeof(T).Name + " component in " + scene.name + " scene.");
            }
            return result;
        }
    }
}

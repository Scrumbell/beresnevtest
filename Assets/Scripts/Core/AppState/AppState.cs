﻿using BeresnevTest.Core.Injection;
using BeresnevTest.Core.EventSystem;
using BeresnevTest.Core.Mvc;
using BeresnevTest.Core.SceneLoading;

namespace BeresnevTest.Core.AppState
{
    public interface IAppState
    {
        void Init(AppStateMachine stateMachine);
        void TransitionIn();
        void TransitionOut();
    }
    public abstract class AppState : IAppState
    {
        [Inject]
        protected IInjector Injector;

        [Inject]
        protected IEventDispatcher EventDispatcher;

        [Inject]
        protected IObjectCreator ObjectCreator;

        [Inject]
        protected ISceneLoader SceneLoader;

        protected AppStateMachine StateMachine;

        public virtual void Init(AppStateMachine stateMachine) {
            StateMachine = stateMachine;
        }

        public virtual void TransitionIn()
        {
            TransitionInFinished();
        }

        public virtual void TransitionOut()
        {
            TransitionOutFinished();
        }

        protected virtual void TransitionInFinished()
        {
            StateMachine.SetTransitionInFinished();
        }

        protected virtual void TransitionOutFinished()
        {
            StateMachine.SetTransitionOutFinished();
        }
    }
}

﻿using BeresnevTest.Game.Common;
using BeresnevTest.Game.Configurations;
using UnityEngine;

namespace BeresnevTest.Game.Actors
{
    public class BallParams : IPhysicsActorParams{
        public Transform ViewContainer { get; private set; }
        public BallStats Stats { get; private set; }
        public BallParams(Transform viewContaner, BallStats stats) {
            ViewContainer = viewContaner;
            Stats = stats;
        }
    }
    public class BallModel : PhysicsActor<BallParams, BallView>
    {
        private IScaleAdapter _scaleAdapter;

        private Vector3 _direction;
        private BallStats _stats;

        protected override void Init(BallParams modelParams)
        {
            base.Init(modelParams);

            CreateModelChild<BallVisualsModel>(new BallVisualsParams(_view.BallVisualsContainer, ObjectCreator.LoadConfiguration<BallColorsConfiguration>().CurrentColor));
            _scaleAdapter = CreateModelChild<TransformScale>(new TransformScaleParams(_view.transform));

            var collisionListener = CreateModelChild<CollisionListener>(new CollisionListenerParams(_view.CollisionElement));
            collisionListener.AddTagListener("Paddle", HorizontalCollisionHandler);
            collisionListener.AddTagListener("HorizontalWall", HorizontalCollisionHandler);
            collisionListener.AddTagListener("VerticalWall", VerticalCollisionHandler);

            Reset();
            SetStats(modelParams.Stats);
        }

        public override void Reset() {
            base.Reset();
            //to avoid launching in weird angles (fully horizontally, fully vertically)
            _direction = new Vector2(Random.Range(0.1f, 2f) * (Random.value > 0.5f ? 1 : -1), Random.value > 0.5f ? 1 : -1).normalized;
        }

        public void SetStats(BallStats stats)
        {
            _stats = stats;
            _scaleAdapter.Set(_stats.Size);
        }

        protected override void UpdateActorImpl(float deltaTime)
        {
            _positionAdapter.Translate(_direction * _stats.Speed * deltaTime);
        }

        private void HorizontalCollisionHandler() {
            _direction = new Vector3(_direction.x, -_direction.y, _direction.z);
        }

        private void VerticalCollisionHandler()
        {
            _direction = new Vector3(-_direction.x, _direction.y, _direction.z);
        }
    }
}

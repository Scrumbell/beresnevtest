﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BeresnevTest.Core.SceneLoading {
    public interface ISceneLoader {
        void Init();
        void LoadScene(string sceneName, Action callback);
        void LoadScene(string sceneName, Action<Scene> callback = null);
        void UnloadScene(string sceneName, Action callback);
        void UnloadScene(string sceneName, Action<Scene> callback = null);
        void Destroy();
    }

    public class SceneLoader : ISceneLoader
    {
        private bool _inited = false;
        private bool _destroyed = false;

        private Dictionary<string, Action<Scene>> _loadingScenes;
        private Dictionary<string, Action<Scene>> _unloadingScenes;
        private List<Scene> _loadedScenes;

        public SceneLoader() {
            _loadingScenes = new Dictionary<string, Action<Scene>>();
            _unloadingScenes = new Dictionary<string, Action<Scene>>();
            _loadedScenes = new List<Scene>();
        }

        public void Init() {
            if (_inited) {
                return;
            }

            SceneManager.sceneLoaded += SceneLoadedHandler;
            SceneManager.sceneUnloaded += SceneUnloadedHandler;

            _inited = true;
        }

        public void LoadScene(string sceneName, Action callback) {
            LoadScene(sceneName, (scene) => { callback?.Invoke(); });
        }

        public void LoadScene(string sceneName, Action<Scene> callback = null)
        {
            if (_loadedScenes.Any(scene => scene.name == sceneName))
            {
                Debug.LogWarning(this + ".LoadScene : Scene " + sceneName + " is already loaded");
            }
            else if (_loadingScenes.ContainsKey(sceneName))
            {
                Debug.LogWarning(this + ".LoadScene : Scene " 
                    + sceneName + " is already being loaded with callback: " 
                    + (_loadingScenes[sceneName] == null ? "(None)" : _loadingScenes[sceneName].ToString()));
            }
            else {
                _loadingScenes.Add(sceneName, callback);
                SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
            }
        }

        public void UnloadScene(string sceneName, Action callback)
        {
            UnloadScene(sceneName, (scene) => { callback?.Invoke(); });
        }

        public void UnloadScene(string sceneName, Action<Scene> callback = null)
        {
            if (!_loadedScenes.Any(scene => scene.name == sceneName))
            {
                Debug.LogWarning(this + ".UnloadScene : Scene " + sceneName + "is not loaded");
            }
            else if (_unloadingScenes.ContainsKey(sceneName))
            {
                Debug.LogWarning(this + ".UnloadScene : Scene " 
                    + sceneName + " is already being unloaded with callback: " 
                    + (_unloadingScenes[sceneName] == null ? "(None)" : _unloadingScenes[sceneName].ToString()));
            }
            else
            {
                _unloadingScenes.Add(sceneName, callback);
                SceneManager.UnloadSceneAsync(sceneName, UnloadSceneOptions.UnloadAllEmbeddedSceneObjects);
            }
        }

        private void SceneLoadedHandler(Scene scene, LoadSceneMode loadSceneMode) {
            _loadedScenes.Add(scene);
            if (_loadingScenes.ContainsKey(scene.name)) {
                var callback = _loadingScenes[scene.name];
                _loadingScenes.Remove(scene.name);
                callback?.Invoke(scene);
            }
        }

        private void SceneUnloadedHandler(Scene scene) {
            _loadedScenes.Remove(scene);
            if (_unloadingScenes.ContainsKey(scene.name))
            {
                var callback = _unloadingScenes[scene.name];
                _unloadingScenes.Remove(scene.name);
                callback?.Invoke(scene);
            }
        }

        private void UnloadAllScenes()
        {
            foreach (Scene scene in _loadedScenes)
            {
                UnloadScene(scene.name);
            }
            _loadedScenes = null;
        }

        private void ClearAllCallbacks()
        {
            _loadingScenes = new Dictionary<string, Action<Scene>>();
            _unloadingScenes = new Dictionary<string, Action<Scene>>();
        }

        public void Destroy() {
            if (_destroyed) {
                return;
            }

            SceneManager.sceneLoaded -= SceneLoadedHandler;
            SceneManager.sceneUnloaded -= SceneUnloadedHandler;

            UnloadAllScenes();
            ClearAllCallbacks();

            _destroyed = true;
        }
    }
}

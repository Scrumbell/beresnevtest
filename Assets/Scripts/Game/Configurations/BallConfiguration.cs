﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Configurations
{
    [CreateAssetMenu(fileName = "BallConfiguration", menuName = "Configurations/Ball")]
    public class BallConfiguration : Configuration
    {
        [SerializeField]
        private float MinSpeed;
        [SerializeField]
        private float MaxSpeed;
        [SerializeField]
        private float MinSize;
        [SerializeField]
        private float MaxSize;

        public BallStats GetStats() {
            return new BallStats(Random.Range(MinSpeed, MaxSpeed), Random.Range(MinSize, MaxSize));
        }
    }
}

﻿using BeresnevTest.Core.Mvc;
using UnityEngine;

namespace BeresnevTest.Game.Locators
{
    public class MenuSceneLocator : View
    {
        public Transform BallContainer;
        public Transform ScreensContainer;
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using BeresnevTest.Core.EventSystem.Events;
using UnityEngine;

namespace BeresnevTest.Core.EventSystem
{
    public interface IEventDispatcher
    {
        void AddListener<T>(Action<T> a) where T : EventBase;

        bool HasListener<T>(Action<T> a) where T : EventBase;

        void RemoveListener<T>(Action<T> a) where T : EventBase;

        void DispatchEvent<T>(T e) where T : EventBase;

        void RemoveAllEvents();

    }

    public class EventDispatcher : IEventDispatcher
    {
        private Dictionary<Type, IList> events;

        public EventDispatcher()
        {
            events = new Dictionary<Type, IList>();
        }

        public void RemoveAllEvents()
        {
            events = new Dictionary<Type, IList>();
        }

        public void AddListener<T>(Action<T> a) where T : EventBase
        {
            Type t = typeof(T);
            if (events.ContainsKey(t))
            {
                IList l = events[t];
                if (l.Contains(a))
                {
                    Debug.LogWarning(this + ".AddListener : Event " + t + " is already registerd with action: " + a);
                }
                else
                {
                    l.Add(a);
                }
            }
            else
            {
                events.Add(t, new List<Action<T>> { a });
            }
        }

        public bool HasListener<T>(Action<T> a) where T : EventBase
        {
            Type t = typeof(T);
            if (events.ContainsKey(t))
            {
                IList l = events[t];
                return l.Contains(a);
            }

            return false;
        }

        public void RemoveListener<T>(Action<T> a) where T : EventBase
        {
            Type t = typeof(T);
            if (events.ContainsKey(t))
            {
                IList l = events[t];
                l.Remove(a);
            }
        }

        public void DispatchEvent<T>(T e) where T : EventBase
        {
            Type t = typeof(T);
            if (events.ContainsKey(t))
            {
                IList l = events[t];
                for (int i = l.Count - 1; i > -1; i--)
                {
                    Action<T> a = (Action<T>)l[i];
                    a.Invoke(e);
                }
            }
        }
    }
}


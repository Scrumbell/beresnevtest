﻿using BeresnevTest.Core.EventSystem.Events;
using BeresnevTest.Game.Data;

namespace BeresnevTest.Game.Events
{
    public class GoalHitEvent : EventBase
    {
        public PlayerNumber ScoredPlayer { get; private set; }

        public GoalHitEvent(PlayerNumber scoredPlayer) {
            ScoredPlayer = scoredPlayer;
        }
    }
}

﻿using BeresnevTest.Core.Mvc;
using BeresnevTest.Game.Events;
using UnityEngine;
using TMPro;

namespace BeresnevTest.Game.UI.Match
{
    public class CountdownTimerParams : IModelParams {
        public TextMeshProUGUI TimerText { get; private set; }

        public CountdownTimerParams(TextMeshProUGUI timerText) {
            TimerText = timerText;
        }
    }
    public class CountdownTimerModel : Model<CountdownTimerParams>
    {
        private TextMeshProUGUI _timerText;

        protected override void Init(CountdownTimerParams modelParams)
        {
            base.Init(modelParams);
            _timerText = modelParams.TimerText;
            _timerText.gameObject.SetActive(false);

            EventDispatcher.AddListener<CountdownTimeChangedEvent>(OnCountdownTimeChanged);
            EventDispatcher.AddListener<CountdownEndedEvent>(OnCountdownEnded);
        }

        private void OnCountdownTimeChanged(CountdownEvent e) {
            _timerText.gameObject.SetActive(true);
            _timerText.text = Mathf.Ceil(e.CountdownTime).ToString();
        }

        private void OnCountdownEnded(CountdownEvent e) {
            _timerText.gameObject.SetActive(false);
        }

        public override void Destroy()
        {
            EventDispatcher.RemoveListener<CountdownTimeChangedEvent>(OnCountdownTimeChanged);
            EventDispatcher.RemoveListener<CountdownEndedEvent>(OnCountdownEnded);

            base.Destroy();
        }
    }
}

﻿using BeresnevTest.Core.Mvc;
using UnityEngine.UI;
using TMPro;

namespace BeresnevTest.Game.Controller.Menu.Screens
{
    public class SettingsScreenView : View
    {
        public TextMeshProUGUI ColorNameText;
        public Button LeftButton;
        public Button RightButton;
        public Button ApplyButton;
        public Button BackButton;
    }
}
